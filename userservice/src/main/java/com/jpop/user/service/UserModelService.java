package com.jpop.user.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jpop.user.dao.UserModelRepository;
import com.jpop.user.model.UserModel;

@Service
public class UserModelService {
	@Autowired
	private UserModelRepository userModelRepository;

	public List<UserModel> getAllUsers() {
		return userModelRepository.findAll();
	}

	public UserModel saveUserModel(UserModel userModel) {
		return userModelRepository.save(userModel);
	}

	public Optional<UserModel> getUserModelById(int id) {
		Optional<UserModel> optionalUserModel = userModelRepository.findById(id);
		return optionalUserModel;
	}

	public void deleteUserModelById(int id) {
		userModelRepository.deleteById(id);
	}

	public UserModel updateUserModel(int id, UserModel userModel) {
		UserModel userModelToUpdate = userModelRepository.getOne(id);
		userModelToUpdate.setUserName(userModel.getUserName());
		userModelToUpdate.setPassword(userModel.getPassword());
		return userModelRepository.save(userModelToUpdate);
	}
}
