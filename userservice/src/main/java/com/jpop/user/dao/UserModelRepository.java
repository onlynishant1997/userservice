package com.jpop.user.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jpop.user.model.UserModel;

@Repository
public interface UserModelRepository extends JpaRepository<UserModel, Integer>{

}
