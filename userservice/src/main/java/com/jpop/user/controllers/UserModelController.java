package com.jpop.user.controllers;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpop.user.model.UserModel;
import com.jpop.user.service.UserModelService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController()
@RequestMapping("/users")
public class UserModelController {
	Logger logger = Logger.getLogger(UserModelController.class.getName());

	@Autowired
	private UserModelService userModelService;

	@ApiOperation(value = "Returns The List of Users")
	@ApiResponses(value = {@ApiResponse(code = 200,message = "Succesfully retrieved users")})
	@GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserModel>> getAllUserModels() {
		return ResponseEntity.ok(userModelService.getAllUsers());
	}

	@ApiOperation(value = "Returns user with particular id")
	@ApiResponses(value = {@ApiResponse(code = 200,message = "Found the user")})
	@GetMapping("/{userId}")
	public ResponseEntity<UserModel> getUserModelById(@PathVariable("userId") int userModel_id) {
		logger.info("Get UserModel------- Id = " + userModel_id);
		Optional<UserModel> optionalUserModel = userModelService.getUserModelById(userModel_id);
		if (!optionalUserModel.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(optionalUserModel.get());
	}

	@ApiOperation(value = "Save the user and returns the saved user")
	@PostMapping(path = "/")
	public ResponseEntity<UserModel> addUserModel(@RequestBody UserModel userModel) {
		logger.info("Add UserModel-------" + userModel.toString());
		return ResponseEntity.ok(userModelService.saveUserModel(userModel));
	}

	@ApiOperation(value = "Deletes the user from database")
	@DeleteMapping("/{userId}")
	public ResponseEntity<?> deleteUserModel(@PathVariable("userId") int userModel_id) {
		logger.info("Delete UserModel ------- Id = " + userModel_id);
		if (!userModelService.getUserModelById(userModel_id).isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		userModelService.deleteUserModelById(userModel_id);
		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Updates the user details in db")
	@PutMapping("/{userId}")
	public ResponseEntity<?> updateUserModel(@PathVariable("userId") int userModel_id, @RequestBody UserModel userModel) {
		logger.info("Update UserModel-------  " + userModel_id);
		if (!userModelService.getUserModelById(userModel_id).isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok(userModelService.updateUserModel(userModel_id,userModel));
	}
}
